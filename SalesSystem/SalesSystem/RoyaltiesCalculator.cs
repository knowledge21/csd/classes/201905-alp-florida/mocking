﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesSystem
{
    public class RoyaltiesCalculator
    {
        private const double ROYALTIES_PERCENTAGE = 0.2;
        SalesRepository repo;

        public RoyaltiesCalculator()
        {
            repo = new SalesRepository();
        }
        public RoyaltiesCalculator(SalesRepository SalesRepo)
        {
            repo = SalesRepo;
        }

        public double Calculate(int month, int year)
        {
         
            ComissionCalculator calc = new ComissionCalculator();
            double salesWithoutCommision = 0.00;
            List<Sale> saleList = repo.GetSales(month, year);
            if (saleList != null)
            {
                foreach (Sale sale in saleList)
                {
                    salesWithoutCommision += sale.value - calc.Calculate(sale.value);
                }
                
            }
            return salesWithoutCommision * ROYALTIES_PERCENTAGE;

        }
    }
}