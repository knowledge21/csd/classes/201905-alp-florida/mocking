using Microsoft.VisualStudio.TestTools.UnitTesting;
using SalesSystem;
using Moq;
using System.Collections.Generic;

namespace SalesSystem.Tests
{
    [TestClass]
    public class RoyaltiesCalculatorTests
    {
        Mock<SalesRepository> fakeRepo;
        List<Sale> listOfSales;

        [TestInitialize]
        public void mockingSetup()
        {
            fakeRepo = new Mock<SalesRepository>();
            listOfSales = new List<Sale>();
        }

        [TestMethod]
        public void TestJan2030WithoutSalesReturn0USDRoyalties()
        {
            int month = 01;
            int year = 2030;
            double expectedRoaylaties = 0;

            var royaltiesCalculator = new RoyaltiesCalculator();

            double actualRoyalties = royaltiesCalculator.Calculate(month, year);

            Assert.AreEqual(expectedRoaylaties, actualRoyalties);
        }
        [TestMethod]
        public void TestJan2019WithOne10KSaleReturn1900USDRoyalties()
        {
            int month = 01;
            int year = 2019;
            double expectedRoaylaties = 1900;
            listOfSales.Add(new Sale(1, 10000));

            fakeRepo.Setup(w => w.GetSales(month, year)).Returns(listOfSales);

            var royaltiesCalculator = new RoyaltiesCalculator(fakeRepo.Object);

            double actualRoyalties = royaltiesCalculator.Calculate(month, year);

            Assert.AreEqual(expectedRoaylaties, actualRoyalties);
        }
        [TestMethod]
        public void TestDec1967With3SaleReturn20890USDRoyalties()
        {
            int month = 12;
            int year = 1967;
            double expectedRoaylaties = 20890;
            
            listOfSales.Add(new Sale(2, 10000));
            listOfSales.Add(new Sale(3, 1000));
            listOfSales.Add(new Sale(4, 100000));

            fakeRepo.Setup(w => w.GetSales(month, year)).Returns(listOfSales);

            var royaltiesCalculator = new RoyaltiesCalculator(fakeRepo.Object);

            double actualRoyalties = royaltiesCalculator.Calculate(month, year);

            Assert.AreEqual(expectedRoaylaties, actualRoyalties);
        }
    }
}